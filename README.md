# s6-linux-utils - tiny Linux-specific utilities
----------------------------------------------

s6-linux-utils is a set of tiny Linux-specific utilities,
some performing well-known tasks such as mount and swapon,
other being nonstandard but typically useful for a s6-based
system. They have been optimized for simplicity and small size.
They were designed for embedded systems and other constrained
environments, but they work on any Linux system.

 Of particular interest in this package are the following
programs:
 - s6-ps, an independent ps implementation

 See https://skarnet.org/software/s6-linux-utils/ for details.


## Installation
  ------------

 See the INSTALL file.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-rc/s6-linux-utils.git && 
cd s6-linux-utils.git && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

```
sudo apt install git-buildpackage skalibs-dev
```

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

These flags will need to be added to the `debian/rules` file.



* Contact information
  -------------------

 Laurent Bercot <ska-skaware at skarnet.org>

 Please use the <skaware at list.skarnet.org> mailing-list for
questions about the inner workings of s6, and the
<supervision at list.skarnet.org> mailing-list for questions
about process supervision, init systems, and so on.
